import React, { Component } from "react";

class Product extends Component {
  render() {
    const { id, name, price, quantity } = this.props.value;

    return (
      <div>
        <h4>
          {" "}
          {name}:Rs.{price}
        </h4>
        <button
          onClick={() => {
            this.props.onDecrement(id);
          }}
        >
          {" "}
          -
        </button>{" "}
        {quantity}{" "}
        <button
          onClick={() => {
            this.props.onIncrement(id);
          }}
        >
          {" "}
          +
        </button>
      </div>
    );
  }
}

export default Product;
