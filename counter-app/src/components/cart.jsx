import React, { Component } from "react";
import Product from "./product";

class Cart extends Component {
  state = {
    products: [
      { id: "1", name: "pen", price: "25", quantity: "5" },
      { id: "2", name: "pencil", price: "5", quantity: "15" },
      { id: "3", name: "eraser", price: "10", quantity: "2" },
      { id: "4", name: "sharpner", price: "15", quantity: "1" },
    ],
  };
  render() {
    return (
      <div>
        {this.state.products.map((product) => (
          <Product
            key={product.id}
            value={product}
            onIncrement={this.handleIncrement}
            onDecrement={this.handleDecrement}
          />
        ))}
      </div>
    );
  }

  handleIncrement = (id) => {
    console.log("handling increment:" + id);
  };

  handleDecrement = (id) => {
    console.log("handling decrement:" + id);
  };
}

export default Cart;
